### What is this repository for? ###

This is my personal repository for files, scripts, and programs that I use and update frequently.

Please read the documentation supplied for each individual document/program, or ask any question in an email.