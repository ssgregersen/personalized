#!/usr/bin/env bash

# *** What is this script? ***
#
# To understand the script, please read the echos and comments.

echo # blank line
echo "Custom *nix flavor install script!"
echo "==================================="
echo # blank line
echo "If possible, this script will try to install only *missing* items."
echo "IMPORTANT: this is not, however, guaranteed. Continue at your own risk."
echo "In fact, this is meant to be targeting freshly installed systems."
echo "Supports Ubuntu on Windows. But *not* Cygwin."
echo # blank line

while true; do
    default="Yes"
    echo -n "Do you wish to continue installing at your own risk? [Y/n] "
    read REPLY </dev/tty; if [ -z "$REPLY" ]; then REPLY=$default; fi
    case "$REPLY" in Y*|y*|J*|j*|N*|n*) break ;; esac
    echo "You need to answer yes or no"
done

case "$REPLY" in
    Y*|y*|J*|j*)
    echo "Install will continue in 3 seconds! To quit press CTRL-C."
    echo # blank line
    sleep 3
    ;;
    N*|n*)
    echo "The install is canceled."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
    ;;
esac

# We need to identify a platform/distribution.
# Determine OS platform
CONFIG_GUESS="http://git.savannah.gnu.org/gitweb/?p=config.git;a=blob_plain;f=config.guess;hb=HEAD"
DISTRO=$(curl -s $CONFIG_GUESS | bash)

case $DISTRO in
x86_64-pc-linux-gnu)
    MACH_DESC="Ubuntu on windows 64-bit (not implemented yet!)"
    SUPPORTED="SUPPORTED"
    ;;
*)
    MACH_DESC="Unknown system"
    SUPPORTED="UNSUPPORTED"
    ;;
esac

echo # blank line
echo "The current distribution is identified as:"
echo "'${DISTRO}' ${MACH_DESC}"

case $SUPPORTED in
UNSUPPORTED) echo "Unfortunately, this means the script cannot process anything."
    echo # blank line
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
    # exit from shell or function but don't exit interactive shell
    ;;
esac

echo # blank line
echo # blank line
echo "Installing a whole bunch of items."
echo "==================================="
echo # blank line

case $DISTRO in
x86_64-pc-linux-gnu)
    # add geomview
    echo -n "Sublime text 3... "
    SUBL_X64="https://download.sublimetext.com/latest/stable/windows/x64/zip"
    echo "100%."
esac


echo # blank line
echo # blank line
echo "Install finished."
echo "=================="
echo # blank line
echo "The install script is now finished!"
echo "Please enjoy your new computing environment."
echo # blank line
echo "Any problems or comments, please let the author/distributor know."
echo # blank line
