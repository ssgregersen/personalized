#!/usr/bin/env bash

# Bash aliases
# =============
#
# A collection of personal aliases, variables, and convenience function
#
# To install use:
# > curl -s https://bitbucket.org/ssgregersen/personalized/raw/master/.bash_aliases > ~/.bash_aliases

alias personal_editor="subl"

dailylog() {
# dailylog opens the log file of today.
# For now, a new log file is empty.

year_month=$(date +%Y_%m)
year_month_day=$(date +%Y_%m_%d)

mkdir -p  ~/workspace/logs/$year_month

if [ ! -f ~/workspace/logs/$year_month/$year_month_day.daily.log ]; then
cat <<'EOF' >> ~/workspace/logs/$year_month/$year_month_day.daily.log

Breif overview
===============



Remaining TODO list:
=====================

# ...


New ideas:
===========

EOF
fi
personal_editor ~/workspace/logs/$year_month/$year_month_day.daily.log
}
